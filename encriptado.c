#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
typedef unsigned long int WORD; 
#define w   128 /* tamano de palabras en bits */
#define r   12 /* number de rondas */
#define b   16 /* number fr bytes en key */
#define c   1 /* numero de palabras en key = ceil(8*b/w)*/
#define t   26 /* tamano de la tabla S = 2*(r+l) words */
WORD S[t]; /* tabla de llaves */
WORD P = 0xb7e15163, Q = 0x9e3779b9; /* constantes magicas */
FILE *fptr;
FILE *fptr2;
#define ROTL(x,y) (((x)<<(y&(w-1))) | ((x)>>(w-(y&(w-1)))))
#define ROTR(x,y) (((x)>>(y&(w-1))) | ((x)<<(w-(y&(w-1)))))

void RC5_ENCRYPT(WORD *pt, WORD *ct) { 
    WORD i, A=pt[0] +S[0] , B=pt[1] +S[1] ;
    for (i=1; i<=r; i++) {
        A = ROTL(A^B,B)+S[2*i] ;
        B = ROTL(B^A,A)+S[2*i+1] ;
    }
    ct[0] = A; ct[1] = B;
}

void RC5_DECRYPT(WORD *ct, WORD *pt){ 
    WORD i, B=ct[1], A=ct[0];
    for (i=r; i>0; i--){
        B = ROTR(B-S[2*i+1] ,A)^A;
        A = ROTR(A-S[2*i] ,B)^B;
    }
    pt[1] = B-S[1]; pt[0] = A-S[0];
}

void RC5_SETUP(unsigned char *K){ 
    WORD i, j, k, u=w/8, A, B, L[c];
    for (i=b-1,L[c-1]=0; i!=-1; i--) L[i/u] = (L[i/u]<<8)+K[i];
    for (S[0]=P,i=1; i<t; i++) S[i] = S[i-1]+Q;
    for (A=B=i=j=k=0; k<3*t; k++,i=(i+1)%t,j=(j+1)%c){ /* 3*t > 3.c */
        A = S[i] = ROTL(S[i]+(A+B),3);
        B = L[j] = ROTL(L[j]+(A+B),(A+B));
    }
} 

void string2hexString(char* input, char* output){
    int loop;
    int i; 
    
    i=0;
    loop=0;
    
    while(input[loop] != '\0')
    {
        sprintf((char*)(output+i),"%02X", input[loop]);
        loop+=1;
        i+=2;
    }
    output[i++] = '\0';
}

int String_to_decimal(char palabra[]){
    int len = strlen(palabra);
    char hex_str[(len*2)+1];
    string2hexString(palabra, hex_str);
    const char *hexstring = hex_str;
    int number = (int)strtol(hex_str, NULL, 16);
    return number;
}

int main(){

    struct timeval start, end;
    gettimeofday(&start, NULL);

    const char* filename = "datos.txt";
    const char* filename2 = "llave.txt";
    

    FILE* input_file = fopen(filename, "r");
    if (!input_file)
        exit(EXIT_FAILURE);
    char *contents = NULL;
    fseek(input_file, 0L, SEEK_END);
    long int res = ftell(input_file);
    fseek(input_file, 0L, SEEK_SET);
    size_t len1 = 0;
    WORD pt1[2],pt2[2];
    int count = 0;
    int h = 0;
    char k[res/4][4] ;
    while (getline(&contents, &len1, input_file) != -1){
        for(int i=0; i<strlen(contents); i=i+4){

        if( i<strlen(contents))
            k[h][0] =  contents[i];
        if( i+1<strlen(contents))
            k[h][1] =  contents[i+1];
        if( i+2<strlen(contents))
            k[h][2] =  contents[i+2];
        if( i+3<strlen(contents))
            k[h][3] =  contents[i+3];
        h=h+1;
    }
    }

    int lector;
    WORD i, j, ct[2] = {0,0};
    unsigned char key[b];
    if (sizeof(WORD)!=4)
        printf("RC5 error: WORD has %d bytes.\n",sizeof(WORD));
    printf("RCS-32/12/16 examples:\n");
    char k1[res/4][4]; 
    strcpy(k1,k);
    char temp[4];
    char temp2[4];
    int ini =0;
    int boleano=0;
    fptr = fopen("encrypt.txt","w");
    fptr2 = fopen("llave.txt","w");
    for(lector=0; lector< res/4 ;lector=lector+1){
        strncpy(temp, k1[lector], 4);
        temp[4] = 0;
        if(ini==1){
            printf("lector+1:  %s\n", temp);
            pt1[1] = String_to_decimal(temp);
            ini = 0;
            boleano = 1;
        }  
        else if(ini==0){
            printf("lector:  %s\n", temp);
            pt1[0] = String_to_decimal(temp);
            ini=1;
            boleano=0;
        }
        if(boleano){
            
            for (i=1;i<2;i++){
            for (j=0;j<b;j++){
            key[j] = ct[0]%(255-j);
            fprintf(fptr2, "%d,", key[j] , j);
            }
            RC5_SETUP(key);
            printf("Valor de key : %d \n",key);
            printf("\nPT1: %.1X \n", *pt1);
            RC5_ENCRYPT(pt1,ct);
            RC5_DECRYPT(ct,pt2);
            printf("\n%d. key = ",i);
            for (j=0; j<b; j++){
            printf("%.2X",key[j]);
            }
            printf("\n plaintext %.1X %.1X ---> ciphertext %.1X %.1X \n", pt1[0], pt1[1], ct[0], ct[1]);
            if (pt1[0]!=pt2[0] || pt1[1]!=pt2[1])
            printf ("Decryption Error ! ");
            printf("ciphertext %.1X %.1X ---> plaintext %.1X %.1X \n", ct[0], ct[1], pt2[0], pt2[1]);

            if(fptr == NULL)
            {
                printf("Error!");   
                exit(1);             
            }

            fprintf(fptr,"%d,",ct[0]);
            fprintf(fptr,"%d,",ct[1]);

        

            

            
            };
        };
    }
    fclose(fptr);
    fclose(fptr2);

    gettimeofday(&end, NULL);
    float seconds = (end.tv_sec - start.tv_sec);
    float micros = ((seconds * 1000000) + end.tv_usec) - (start.tv_usec);
    float secondsF = micros/1000000;
    printf("El tiempo de ejecucion es: %f seconds\n", secondsF);
    system("git add -A");
    system("git commit -m 'se-envia-el-texto-cifrado'");
    system("git push -u origin main");
    return 0;
}
