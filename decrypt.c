#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
typedef unsigned long int WORD;
#define w   128 /* tamano de word en bits */
#define r   12 /* number de rondas*/
#define b   16 /* number de bytes en key */
#define c   1 /* number de palabras en key = ceil(8*b/w)*/
#define t   26 /* tamano de la tabla S*/
WORD S[t]; /* tabla de llaves */
WORD P = 0xb7e15163, Q = 0x9e3779b9; /* constantes magicas */
FILE *fptr;
FILE *fptr2;
FILE *fptr3;
#define ROTL(x,y) (((x)<<(y&(w-1))) | ((x)>>(w-(y&(w-1)))))
#define ROTR(x,y) (((x)>>(y&(w-1))) | ((x)<<(w-(y&(w-1)))))

struct timeval {
   time_t      tv_sec;   // Number of whole seconds of elapsed time
   long int    tv_usec;  // Number of microseconds of rest of elapsed time minus tv_sec. Always less than one million
};

void RC5_DECRYPT(WORD *ct, WORD *pt){ 
    WORD i, B=ct[1], A=ct[0];
    for (i=r; i>0; i--){
        B = ROTR(B-S[2*i+1] ,A)^A;
        A = ROTR(A-S[2*i] ,B)^B;
    }
    pt[1] = B-S[1]; pt[0] = A-S[0];
}

void RC5_SETUP(unsigned char *K){ 
    WORD i, j, k, u=w/8, A, B, L[c];
    for (i=b-1,L[c-1]=0; i!=-1; i--) L[i/u] = (L[i/u]<<8)+K[i];
    for (S[0]=P,i=1; i<t; i++) S[i] = S[i-1]+Q;
    for (A=B=i=j=k=0; k<3*t; k++,i=(i+1)%t,j=(j+1)%c){ /* 3*t > 3.c */
        A = S[i] = ROTL(S[i]+(A+B),3);
        B = L[j] = ROTL(L[j]+(A+B),(A+B));
    }
} 

int main(){
    struct timeval start, end;
    gettimeofday(&start, NULL);

    size_t len1 = 0;
    WORD pt1[2],pt2[2];
    int count = 0;
    int lector;
    WORD i, j, ct[2] = {0,0};
    unsigned char key[b];
    
    if (sizeof(WORD)!=4)
    printf("RC5 error: WORD has %d bytes.\n",sizeof(WORD));
    int ini =0;
    int boleano=0;
    int contador=0;
    fptr = fopen("encrypt.txt","r");
    fptr2 = fopen("llave.txt","r");
    fseek(fptr, 0L, SEEK_END);
    long int res = ftell(fptr);
    fseek(fptr, 0L, SEEK_SET);


    char bufer[10000];         // Aquí vamos a ir almacenando cada línea
    while (fgets(bufer, 10000, fptr))
    {
        // Aquí, justo ahora, tenemos ya la línea. Le vamos a remover el salto
        strtok(bufer, "\n");
    }

    char bufer2[10000];         // Aquí vamos a ir almacenando cada línea
    while (fgets(bufer2, 10000, fptr2))
    {
        // Aquí, justo ahora, tenemos ya la línea. Le vamos a remover el salto
        strtok(bufer2, "\n");
    }
    int counter=0;
    int cantidad_de_bloques = res/11 + 1;
    WORD texto_extraido[cantidad_de_bloques];
    WORD texto_extraido2[cantidad_de_bloques*32];
    char * token = strtok(bufer, ",");
    
    
    for(counter=0;counter< cantidad_de_bloques ;counter++){
    texto_extraido[counter]=atoi(token);
    token = strtok(NULL, ","); 
    }
    int counter2=0;
    char * token2 = strtok(bufer2, ",");

    for(counter2=0;counter2< cantidad_de_bloques*8 ;counter2++){
    texto_extraido2[counter2]=atoi(token2);
    token2 = strtok(NULL, ","); 
    }
    int contador2=0;
    
    int llaves = 0;
    int x =0;
    int contador_de_bloques;
    fptr3 = fopen("textoplano.txt","w");

    for(contador_de_bloques=0;   contador_de_bloques < cantidad_de_bloques  ;  contador_de_bloques= contador_de_bloques+2  ){
        ct[0]=texto_extraido[contador_de_bloques];
        ct[1]=texto_extraido[contador_de_bloques+1];

        for(llaves= contador_de_bloques*8 ;llaves < 16 + contador_de_bloques*8 ;llaves++ ){
                        key[x] = texto_extraido2[llaves];
                        x=x+1;
        }
        x=0;
        RC5_SETUP(key);
        RC5_DECRYPT(ct,pt2);
        
        printf("ciphertext %.1X %.1X ---> plaintext %.1X %.1X \n", ct[0], ct[1],pt2[0],pt2[1]  );
        
        if(fptr == NULL)
            {
                printf("Error!");   
                exit(1);             
            };
        fprintf(fptr3,"%.1X",pt2[0]);
        fprintf(fptr3,"%.1X",pt2[1]);   
        pt2[0]=0;
        pt2[1]=0;
        fprintf(fptr3,"\n");
        };

    fclose(fptr);
    fclose(fptr2);
    fclose(fptr3);

    gettimeofday(&end, NULL);
    float seconds = (end.tv_sec - start.tv_sec);
    float micros = ((seconds * 1000000) + end.tv_usec) - (start.tv_usec);
    float secondsF = micros/1000000;
    printf("El tiempo de ejecucion es: %f seconds\n", secondsF);

    system("git add -A");
    system("git commit -m 'se-envia-el-texto-descifrado'");
    system("git push -u origin main");
    
    return 0;

}

